��    9      �  O   �      �     �     �     �  	               >        [     u     }     �  
   �     �  
   �  	   �     �     �  	   �     �     �     �     �       
        (     ,     >     K     _       	   �     �  #   �  
   �     �     �     �          &  +   E     q     w  	   �     �     �     �  	   �  	   �  
   �     �     �  	   �  	   �     �     �  
   �  k  	     u	  	   �	     �	     �	     �	     �	  G   �	  #   �	     #
     +
     3
     :
     F
  	   `
  	   j
     t
     }
     �
     �
     �
  	   �
     �
     �
  	   �
     �
     �
     �
     	          9  	   B     L  "   g  
   �     �     �     �     �      �  #        5     ;     C     L     O     V     ^     g  	   p     z     �  	   �  
   �     �     �     �             0       !                              ,                               $                      	               #   '   9   3   1               6      .          &   
          +   8       5            /      %   -   "          (      )          7      *                2   4    360° panorama About Add Altitude: Bearing: Cancel Celutz is a tool for managing and referencing panoramic photos Celutz, a panorama viewer Control Controls Delete Elevation: Enter an address In degrees In meters Insert Language Latitude: Legend Localized point Locate Locate GPS point Locate existing point Longitude: Map Name of the point New panorama New reference point No panorama seeing this point ! Panorama Panoramas Panoramas seeing the point Panoramas seeing the searched point Parameters Project homepage Reference point Result of research Results for the point Results for the searched point Whether the panorama loops around the edges Zoom: altitude altitude: at image latitude latitude: longitude longitude: name panorama panoramas reference reference point reference points references Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-01 20:51+0000
PO-Revision-Date: 2016-08-31 17:28+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Panorama de 360° Acerca de Añadir Altitud: Dirección: Cancelar Celutz es una heramienta para gestionar y referenciar fotos panoramicas Celutz, un visualisador de panorama Control Control Borrar Elevación: Introducir una dirección En grados En metros Insertar Idioma Latitud: Leyenda Punto localizado Localizar Localizar por GPS Localizar un punto Longitud: Mapa Nombre del punto Añadir un panorama Añadir un punto Ningún panorama ve este punto Panorama Panoramas Panoramas que ven el punto Panoramas que ven el punto deseado Parámetro Pagina del projecto Punto de referencia Resultado de la búsqueda Resultados para el punto Resultados para el punto deseado Si el panorama vuelta en los bordes Zoom: altitud altitud: de imagen latitud latitud: longitud longitud: nombre panorama panoramas referencia punto de referencia puntos de referencia referencias 